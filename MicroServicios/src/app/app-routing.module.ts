import { NgModule, ContentChildren } from '@angular/core';
import { Routes, RouterModule, ChildrenOutletContexts } from '@angular/router';
import { PeliculasComponent } from './components/peliculas/peliculas.component';
import { MusicaComponent } from './components/musica/musica.component';
import { AuthGuard } from './services/auth.guard';
import { SearchArtistaComponent } from './components/musica/search-artista/search-artista.component';
import { ArtistaComponent } from './components/musica/artista/artista.component';
import { InfoPersonalComponent } from './components/info-personal/info-personal.component';
import { SearchPeliculaComponent } from './components/peliculas/search-pelicula/search-pelicula.component';
import { TituloComponent } from './components/peliculas/titulo/titulo.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { HomeComponent } from './components/home/home.component';


const routes: Routes = [
  { path: 'inicio', component: InicioComponent },
  { path: 'home', component: HomeComponent,  canActivate: [ AuthGuard ] },
  { path: 'musica', component: MusicaComponent, canActivate: [ AuthGuard ] },
  { path: 'musica/buscar-artista', component: SearchArtistaComponent, canActivate: [ AuthGuard ] },
  { path: 'musica/artist/:id', component: ArtistaComponent, canActivate: [ AuthGuard ] },
  { path: 'peliculas', component: PeliculasComponent, canActivate: [ AuthGuard ] },
  { path: 'peliculas/buscar-pelicula', component: SearchPeliculaComponent, canActivate: [ AuthGuard ] },
  { path: 'peliculas/titulo/:id', component: TituloComponent, canActivate: [ AuthGuard ] },
  { path: 'info-personal', component: InfoPersonalComponent, canActivate: [ AuthGuard ] },
  { path: '**', pathMatch: 'full', redirectTo: 'inicio' }
];

@NgModule({
  // imports: [RouterModule.forRoot(routes, { useHash: true })],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})export class AppRoutingModule { }
