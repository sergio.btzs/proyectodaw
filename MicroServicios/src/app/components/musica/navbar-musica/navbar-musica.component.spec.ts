import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarMusicaComponent } from './navbar-musica.component';

describe('NavbarMusicaComponent', () => {
  let component: NavbarMusicaComponent;
  let fixture: ComponentFixture<NavbarMusicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavbarMusicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarMusicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
