import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar-musica',
  templateUrl: './navbar-musica.component.html',
  styleUrls: ['./navbar-musica.component.css']
})
export class NavbarMusicaComponent implements OnInit {

  constructor( public auth: AuthService,
               private router: Router) {}

  ngOnInit(): void {
  }

}
