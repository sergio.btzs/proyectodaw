import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tarjetas-musica',
  templateUrl: './tarjetas-musica.component.html',
  styleUrls: ['./tarjetas-musica.component.css']
})
export class TarjetasMusicaComponent {


  @Input() items: any[] = [];

  constructor( private router: Router) { }

  verArtista( item: any) {
    let artistaId;

    if (item.type === 'artist') {
      artistaId = item.id;
      // console.log(artistaId);
    } else {
      artistaId = item.artists[0].id;
      // console.log(artistaId);
    }

    this.router.navigate(['musica/artist', artistaId]);

  }

}
