import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TarjetasMusicaComponent } from './tarjetas-musica.component';

describe('TarjetasMusicaComponent', () => {
  let component: TarjetasMusicaComponent;
  let fixture: ComponentFixture<TarjetasMusicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TarjetasMusicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TarjetasMusicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
