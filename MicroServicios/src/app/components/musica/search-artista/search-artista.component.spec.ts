import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchArtistaComponent } from './search-artista.component';

describe('SearchArtistaComponent', () => {
  let component: SearchArtistaComponent;
  let fixture: ComponentFixture<SearchArtistaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchArtistaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchArtistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
