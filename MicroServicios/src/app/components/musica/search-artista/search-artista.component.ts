import { Component } from '@angular/core';
import { SpotifyService } from '../../../services/spotify.service';
import { Location } from '@angular/common';


@Component({
  selector: 'app-search-artista',
  templateUrl: './search-artista.component.html',
  styleUrls: ['./search-artista.component.css']
})
export class SearchArtistaComponent {

  artistas: any [] = [];
  loading: boolean;

  constructor( private spotify: SpotifyService,
               private location: Location ) {}

  buscar( termino: string ) {

    if (termino.length > 0) {
      this.loading = true;

      console.log(termino);
      this.spotify.getArtistas(termino)
        .subscribe( (data: any) => {
        console.log(data);
        this.artistas = data;
        this.loading = false;
        });
    } else {
      console.log('no escribiste nada');
    }

  }

  goBack() {
    this.location.back();
  }

}
