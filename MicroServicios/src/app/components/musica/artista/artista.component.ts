import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from '../../../services/spotify.service';
import { AuthService } from '../../../services/auth.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styleUrls: ['./artista.component.css']
})
export class ArtistaComponent {

  artista: any = {};
  topTracks: any [] = [];

  loadingArtist: boolean;

  constructor( private router: ActivatedRoute,
               private spotify: SpotifyService,
               public auth: AuthService,
               private location: Location ) {

    this.loadingArtist = true;

    this.router.params.subscribe( params => {
      console.log(params.id);

      this.getArtista(params.id);
      this.getTopTracks(params.id);
    });
   }

  getArtista( id: string ) {
    this.spotify.getArtista( id ).subscribe( artista => {
      console.log(artista);
      this.artista = artista;
      this.loadingArtist = false;
    });
  }

  getTopTracks( id: string ) {
    this.spotify.getTopTracks( id ).subscribe( topTracks => {
      console.log(topTracks);
      this.topTracks = topTracks;
    });
  }

  goBack() {
    this.location.back();
  }

}
