import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { TmdbService } from '../../../services/tmdb.service';

@Component({
  selector: 'app-search-pelicula',
  templateUrl: './search-pelicula.component.html',
  styleUrls: ['./search-pelicula.component.css']
})
export class SearchPeliculaComponent implements OnInit {

  peliculas: any [] = [];
  loading: boolean;


  constructor( private tmdb: TmdbService ,
               private location: Location ) { }

  ngOnInit(): void {
  }

  buscar( termino: string ) {

    if (termino.length > 0) {
      this.loading = true;

      console.log(termino);
      this.tmdb.getBusquedaTitulo(termino)
        .subscribe( (data: any) => {
        console.log(data);
        this.peliculas = data;
        this.loading = false;
        });
    } else {
      console.log('no escribiste nada');
    }

  }

  goBack() {
    this.location.back();
  }

}
