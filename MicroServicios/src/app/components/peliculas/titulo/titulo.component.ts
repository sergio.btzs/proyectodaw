import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { TmdbService } from '../../../services/tmdb.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-titulo',
  templateUrl: './titulo.component.html',
  styleUrls: ['./titulo.component.css']
})
export class TituloComponent {

  pelicula: any = {};
  casting1: string;
  casting2: string;
  casting3: string;
  genero: string;

  path = 'http://image.tmdb.org/t/p/w400';

  loadingPelicula: boolean;

  constructor( private router: ActivatedRoute,
               private tmdb: TmdbService,
               public auth: AuthService,
               private location: Location ) {

    this.loadingPelicula = true;

    this.router.params.subscribe( params => {
      console.log(params.id);

      this.getPeliculaById(params.id);
    });
   }

   getPeliculaById( id: string ) {
    this.tmdb.getPeliculaById( id ).subscribe( pelicula => {
      console.log(pelicula);
      this.pelicula = pelicula;
      this.genero = pelicula.genres[0].name;

    });

    this.tmdb.getCasting(id).subscribe ( casting => {
      console.log(casting);
      this.casting1 = casting[0].name;
      this.casting2 = casting[1].name;
      this.casting3 = casting[2].name;
    });

    this.loadingPelicula = false;

  }

  goBack() {
    this.location.back();
  }

}
