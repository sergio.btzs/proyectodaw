import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tarjetas-peliculas',
  templateUrl: './tarjetas-peliculas.component.html',
  styleUrls: ['./tarjetas-peliculas.component.css']
})
export class TarjetasPeliculasComponent {

  @Input() items: any[] = [];

  path = 'http://image.tmdb.org/t/p/w400';

  constructor( private router: Router ) { }

  verPelicula( item: any) {
    let peliculaId;

    peliculaId = item.id;

    this.router.navigate(['peliculas/titulo/', peliculaId]);
  }

}
