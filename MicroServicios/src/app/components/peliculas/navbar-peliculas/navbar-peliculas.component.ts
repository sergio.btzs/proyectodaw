import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-navbar-peliculas',
  templateUrl: './navbar-peliculas.component.html',
  styleUrls: ['./navbar-peliculas.component.css']
})
export class NavbarPeliculasComponent implements OnInit {

  constructor( public auth: AuthService) { }

  ngOnInit(): void {
  }

}
