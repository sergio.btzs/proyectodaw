import { Component} from '@angular/core';
import { TmdbService } from '../../services/tmdb.service';

@Component({
  selector: 'app-peliculas',
  templateUrl: './peliculas.component.html',
  styleUrls: ['./peliculas.component.css']
})
export class PeliculasComponent {

  peliculas: any[] = [];
  path = 'http://image.tmdb.org/t/p/w400';
  loading: boolean;
  error: boolean;
  mensajeError: string;

  constructor(private tmdb: TmdbService) {
    this.loading = true;
    this.error = false;

    this.tmdb.getPeliculas().subscribe( (data: any) => {

      console.log(data);

      this.peliculas = data;
      this.loading = false;

    }, ( errorServicio ) => {
      this.loading = false;
      this.error = true;
      console.log(errorServicio.error.error.message);
      this.mensajeError = errorServicio.error.error.message;
    });

  }

}
