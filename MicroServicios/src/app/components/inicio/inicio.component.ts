import { Component } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';
import { TmdbService } from '../../services/tmdb.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent {

loading: boolean;
album: any = {};
artista: string;
imagenAlbum: string;
pelicula: any = {};
imagenPelicula: string;

constructor( private spotify: SpotifyService,
             private tmdb: TmdbService) {

  this.loading = true;

  this.spotify.getRecomendadas().subscribe( (data: any) => {
    console.log(data);
    this.album = data;
    this.artista = data.artists[0].name;

    if (data.images[0].url == null) {
      this.imagenAlbum = 'assets/img/no-image.png';
    } else {
      this.imagenAlbum = data.images[0].url;
    }
    this.loading = false;
    });

  this.tmdb.getUltimaPelicula().subscribe( (data: any) => {
    console.log(data);
    this.pelicula = data;
    if (data.poster_path == null) {
      this.imagenPelicula = 'assets/img/no-image.png';
    } else {
      this.imagenPelicula = 'http://image.tmdb.org/t/p/w400' + data.poster_path;
    }
    this.loading = false;
  });

  }

  registrateAlbum() {
    Swal.fire('Acceda a través de Auth0 y podrá ver toda la información de este álbum');
  }

  registratePelicula() {
    Swal.fire('Acceda a través de Auth0 y podrá ver toda la información de esta película');
  }
}
