import { Component, OnInit, Input } from '@angular/core';
import { NgForm, ControlContainer } from '@angular/forms';
import { PropioService } from '../../../services/propio.service';
import { AuthService } from '../../../services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  sub: string;
  imagen: string;

  Nombre: string;
  Apellidos: string;
  Correo: string;
  Movil: string;
  Notificaciones: boolean;

  nom: string;
  ape: string;
  cor: string;
  mov: string;
  not: boolean;

  datosUsuario: boolean = false;
  formNuevo: boolean = false;
  formModificar: boolean = false;
  nuevo: boolean = false;

  constructor(public auth: AuthService,
              public propio: PropioService) {
  }

  ngOnInit() {
    console.log('entra al OnInit');
    this.auth.userProfile$.subscribe ( (perfil: any) => {
      console.log(perfil);
      this.sub = perfil.sub;
      this.imagen = perfil.picture;
    });

    this.propio.getUsuario(this.sub).subscribe( (data: any) => {
    console.log(data);

    if (data.error > 0) {
          console.log('usuario no existe en bd');
          this.formNuevo = true;
      } else {
        this.datosUsuario = true;
        this.Nombre = data.Nombre;
        this.Apellidos = data.Apellidos;
        this.Correo = data.Correo;
        this.Movil = data.Movil;
        this.Notificaciones = data.Notificaciones;
      }
    });
  }


  // Agrega Datos de usuario a la bd vinculados al Auth0
  guardarNuevo( forma: NgForm ) {
    // console.log(forma);
    // console.log(forma.value);

    if ( forma.invalid ) {
      Object.values( forma.controls ).forEach( control => {
        // console.log ( control );
        control.markAllAsTouched();
      });
      return;
    } else {
      console.log('se ejecuta método agregarUsuario');

      this.nom = forma.value.nombre;
      this.ape = forma.value.apellidos;
      this.cor = forma.value.correo;
      this.mov = forma.value.movil;
      this.not = forma.value.notificaciones;

      this.propio.agregarUsuario(this.sub, this.nom, this.ape, this.cor, this.mov, this.not=false)
                 .subscribe( (data: any) => {
        console.log(data);

        if (data.error == 0) {
          this.formNuevo = false;
          this.datosUsuario = true;

          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Usuario añadido correctamente!',
            showConfirmButton: false,
            timer: 1500
          });

          this.propio.getUsuario(this.sub).subscribe( (data: any) => {
            console.log(data);

            this.Nombre = data.Nombre;
            this.Apellidos = data.Apellidos;
            this.Correo = data.Correo;
            this.Movil = data.Movil;
            this.Notificaciones = data.Notificaciones;
          });
        }

      });
    }
  }


  // modifica Datos de usuario en BD vinculados al Auth0
  guardarModificar( forma: NgForm ) {
    if ( forma.invalid ) {
      Object.values( forma.controls ).forEach( control => {
        // console.log ( control );
        control.markAllAsTouched();
      });
      return;
    } else {
      console.log('se ejecuta método modificar usuario');

      this.nom = forma.value.nombre;
      this.ape = forma.value.apellidos;
      this.cor = forma.value.correo;
      this.mov = forma.value.movil;
      this.not = forma.value.notificaciones;

      this.propio.modificarUsuario(this.sub, this.nom, this.ape, this.cor, this.mov, this.not)
                 .subscribe( (data: any) => {
        console.log(data);

        if (data.error == 0) {
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Datos modificados correctamente!',
            showConfirmButton: false,
            timer: 1500
          });

          this.formModificar = false;
          this.datosUsuario = true;
          this.propio.getUsuario(this.sub).subscribe( (data: any) => {
            console.log(data);

            this.Nombre = data.Nombre;
            this.Apellidos = data.Apellidos;
            this.Correo = data.Correo;
            this.Movil = data.Movil;
            this.Notificaciones = data.Notificaciones;
          });
        }

      });
    }
  }


  //  marca como borrado al usuario en la bd
  eliminarUsuario() {

    Swal.fire({
      title: 'Estás seguro de que quieres eliminar tu cuenta?',
      text: 'Tus datos serán eliminados!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
    }).then((result) => {
      if (result.value) {
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: 'Usuario borrado',
          showConfirmButton: false,
          timer: 1500
        });

        this.auth.logout();
        this.propio.eliminarUsuario(this.sub)
             .subscribe( (data: any) => {
          console.log(data);
        });

      }

    });
  }

}
