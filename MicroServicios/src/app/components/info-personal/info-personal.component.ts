import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { PropioService } from '../../services/propio.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-info-personal',
  templateUrl: './info-personal.component.html',
  styleUrls: ['./info-personal.component.css']
})
export class InfoPersonalComponent implements OnInit {

  mostrar_links: boolean =  false;
  sub: string;
  sub_sergio = 'google-oauth2|110966353621668120859';
  nombre: string;
  // apellidos: string;

  // perfil: any;

  constructor( public auth: AuthService,
               private location: Location,
               public propio: PropioService) {}

   ngOnInit() {
    console.log('entra al OnInit');
    this.auth.userProfile$.subscribe ( (perfil: any) => {
      console.log(perfil);
      this.sub = perfil.sub;
      this.nombre = perfil.given_name;
    });

    if (this.sub == this.sub_sergio) {
      this.mostrar_links = false;
    }

  }

  goBack() {
    this.location.back();
  }

}
