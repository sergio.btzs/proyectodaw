import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
import { SpotifyService } from '../../../services/spotify.service';
import { TmdbService } from '../../../services/tmdb.service';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor( public auth: AuthService,
               private spotify: SpotifyService,
               private tmdb: TmdbService) {
               }

  ngOnInit(): void {
  }

}
