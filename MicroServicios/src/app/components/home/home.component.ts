import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  nombre: string;

  constructor(public auth: AuthService) {}

  ngOnInit() {
  console.log('entra al OnInit');
  this.auth.userProfile$.subscribe ( (perfil: any) => {
      console.log(perfil);
      this.nombre = perfil.given_name;
    });

  }
}
