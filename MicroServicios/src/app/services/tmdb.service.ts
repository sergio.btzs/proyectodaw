import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TmdbService {

  n: number = Math.round(Math.random() * (19 - 1) + 1);
  apiKey = '3e237e85d87cb3fe615c9233e8f65266';

  constructor( private http: HttpClient) {
    console.log('Servicio The Movie Data Base listo!');
  }

  // construye la query
  getQuery( query: string) {
    const url = `https://api.themoviedb.org/3/${ query }`;
    return this.http.get(url);
  }

  // devuelve un listado de las películas ordenadas por su popularidad
  getPeliculas() {
    return this.getQuery(`discover/movie?api_key=${this.apiKey}&language=es-ES&`
      + `sort_by=popularity.desc&include_adult=false&include_video=false&page=1`)
                .pipe( map ( (data: any) =>
                data.results
                ));
  }

  // devuelve un listado de peliculas por su nombre
  getBusquedaTitulo( termino: string ) {
    return this.getQuery(`search/movie?api_key=${this.apiKey}` +
                          `&language=es-Es&query=${ termino }&page=1&include_adult=false`)
                .pipe(map( (data: any) =>
                data.results
                ));
  }


  // devuelve la información de una película por su id
  getPeliculaById(id: string) {
    return this.getQuery(`movie/${id}?api_key=${this.apiKey}` +
                          `&language=es-Es`)
                    .pipe( map( (data: any) =>
                    data
                    ));
  }

  // devuelve una película al azar que está ahora en cartelera
  getUltimaPelicula() {
    return this.getQuery(`movie/now_playing?api_key=${this.apiKey}` +
                          `&language=es-Es`)
                    .pipe( map( (data: any) =>
                    data.results[this.n]
                    ));
  }

  // devuelve el casting de actores y actrices de una película por su id
  getCasting(id: string) {
    return this.getQuery(`movie/${id}/credits?api_key=${this.apiKey}` +
                          `&language=es-Es`)
                    .pipe( map( (data: any) =>
                    data.cast
                    ));
  }

}
