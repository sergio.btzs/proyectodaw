import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor( private http: HttpClient) {
    console.log('Servicio Spotify listo!');
   }

  // construye la query
  getQuery( query: string) {
    const apiToken = 'BQC1MsIyP3rdIPblRIgRgi-coDpMUOYAFhqTpRNBzPpsNVKdrLaGKLRc-MQeay3YeAjPyvdqr815RhSjtJA';
    const url = `https://api.spotify.com/v1/${ query }`;
    const headers = new HttpHeaders ({ Authorization : 'Bearer ' + apiToken });
    return this.http.get(url, { headers });
  }

  // devuelve los 20 últimos lanzamientos
  getNewReleases() {
    return this.getQuery(`browse/new-releases?limit=20`)
    .pipe( map ( (data: any) => data.albums.items ));
  }

  // busca artistas por nombre
  getArtistas(termino: string) {
    return this.getQuery(`search?q=${ termino }&type=artist&limit=20`)
               .pipe( map ( (data: any) => data.artists.items ));
  }

  // devuelve la información de un artista por su id
  getArtista( id: string ) {
    return this.getQuery(`artists/${ id }`);
              //  .pipe( map ( (data: any) => data ));
  }

  // devuelve las mejores canciones de un artista
  getTopTracks( id: string ) {
    return this.getQuery(`artists/${ id }/top-tracks?country=us`)
                .pipe( map( data => data['tracks']));
  }

  // devuelve un albúm rock
  getRecomendadas() {
    return this.getQuery(`recommendations?limit=10&market=US&seed_genres=rock`)
              .pipe( map ( (data: any) => data.tracks[0].album));

  }

}
