import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PropioService {

  constructor( private http: HttpClient) {
    console.log('Servicio Propio listo!');
   }

    // método que devuelve datos de usuario
    getUsuario(idAuth0: string) {
      return this.http.post(`https://localhost:5001/usuario/detalle`,
        {
          "IdAuth0" : idAuth0
        })
                      .pipe( map ( (data: any) =>
                      data
                      ));
  }

  // método que devuelve agrega un usuario a la BD
  agregarUsuario(idAuth0: string, nombre: string, apellidos: string, correo: string, movil: string, notificaciones: boolean) {
    return this.http.post(`https://localhost:5001/usuario/agregar`,
      {
        "IdAuth0" : idAuth0,
        "Nombre" : nombre,
        "Apellidos" : apellidos,
        "Correo": correo,
        "Movil": movil,
        "Notificaciones": notificaciones
      })
                    .pipe( map ( (data: any) =>
                    data
                    ));
  }

  // método que modifica un usuario de la BD
  modificarUsuario(idAuth0: string, nombre: string, apellidos: string, correo: string, movil: string, notificaciones: boolean) {
    return this.http.post(`https://localhost:5001/usuario/modificar`,
      {
        "IdAuth0" : idAuth0,
        "Nombre" : nombre,
        "Apellidos" : apellidos,
        "Correo": correo,
        "Movil": movil,
        "Notificaciones": notificaciones
      })
                    .pipe( map ( (data: any) =>
                    data
                    ));
  }

  // método que marca como borrado un usuario de la BD
  eliminarUsuario(idAuth0: string) {
    return this.http.post(`https://localhost:5001/usuario/eliminar`,
    {
      "IdAuth0" : idAuth0,
    })
                    .pipe( map ( (data: any) =>
                    data
                    ));
  }

}
