import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


// Componentes
import { AppComponent } from './app.component';
import { PeliculasComponent } from './components/peliculas/peliculas.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { MusicaComponent } from './components/musica/musica.component';
import { NavbarMusicaComponent } from './components/musica/navbar-musica/navbar-musica.component';
import { LoadingComponent } from './components/shared/loading/loading.component';
import { SearchArtistaComponent } from './components/musica/search-artista/search-artista.component';
import { TarjetasMusicaComponent } from './components/musica/tarjetas-musica/tarjetas-musica.component';
import { ArtistaComponent } from './components/musica/artista/artista.component';
import { InfoPersonalComponent } from './components/info-personal/info-personal.component';
import { NavbarPeliculasComponent } from './components/peliculas/navbar-peliculas/navbar-peliculas.component';
import { SearchPeliculaComponent } from './components/peliculas/search-pelicula/search-pelicula.component';
import { TarjetasPeliculasComponent } from './components/peliculas/tarjetas-peliculas/tarjetas-peliculas.component';
import { TituloComponent } from './components/peliculas/titulo/titulo.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { FormularioComponent } from './components/info-personal/formulario/formulario.component';
import { HomeComponent } from './components/home/home.component';


// Pipes
import { NoimagePipe } from './pipes/noimage.pipe';
import { DomseguroPipe } from './pipes/domseguro.pipe';
import { PeliculaImagenPipe } from './pipes/pelicula-imagen.pipe';
import { BooleanoPipe } from './pipes/booleano.pipe';
import { PieComponent } from './components/shared/pie/pie.component';


@NgModule({
  declarations: [
    AppComponent,
    PeliculasComponent,
    NavbarComponent,
    MusicaComponent,
    NavbarMusicaComponent,
    LoadingComponent,
    SearchArtistaComponent,
    NoimagePipe,
    TarjetasMusicaComponent,
    ArtistaComponent,
    DomseguroPipe,
    PeliculaImagenPipe,
    InfoPersonalComponent,
    NavbarPeliculasComponent,
    SearchPeliculaComponent,
    TarjetasPeliculasComponent,
    TituloComponent,
    InicioComponent,
    FormularioComponent,
    HomeComponent,
    BooleanoPipe,
    PieComponent,
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
