import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'booleano'
})
export class BooleanoPipe implements PipeTransform {

  transform(notificaciones: any): any {
    if ( notificaciones ) {
      return 'SI';
    } else if (!notificaciones) {
      return 'NO';
    } else {
      return '';
    }
  }
}
