﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using proyecto.Models;
using proyecto.DB;
using Microsoft.AspNetCore.Cors;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace proyecto.Controllers
{
    [Route("api/[controller]")]
    public class BusquedaController : Controller
    {
        // GET: api/<controller>
        [HttpGet]
        public string Busqueda(int id)
        {
            Mascota InformacionMascota = new Mascota();
            try
            {
                InformacionMascota = WebAPI.BuscarPorId();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var jsonConver = JsonConvert.SerializeObject(InformacionMascota);
            return jsonConver.ToString();
        }
    }
}