﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace proyecto.Models
{
    public class Mascota
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string especie { get; set; }
        public string genero { get; set; }
    }
}