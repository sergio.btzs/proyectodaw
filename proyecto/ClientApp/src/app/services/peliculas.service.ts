import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PeliculasService {

  constructor( private http: HttpClient ) {
    console.log('Servicio peliculas listo!');
   }

  getQuery() {

    const url = `https://api.themoviedb.org/3/discover/movie?` +
      `api_key=3e237e85d87cb3fe615c9233e8f65266&language=es-ES&sort_by=popularity.desc&include_adult=false&include_video=false&page=1`;

    return this.http.get(url).pipe( map ( (data: any) => console.log(data) ) );
  }

}
