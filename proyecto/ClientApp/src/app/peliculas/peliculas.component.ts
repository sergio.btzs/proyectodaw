import { Component, OnInit } from '@angular/core';
import { PeliculasService } from '../services/peliculas.service';

@Component({
  selector: 'app-peliculas',
  templateUrl: './peliculas.component.html'
})
export class PeliculasComponent implements OnInit {


  listado: any[] = [];
  pagina: number;
  prueba = 'prueba';

  constructor( private peliculas: PeliculasService) {
    // this.getQuery();


  this.peliculas.getQuery().subscribe( (data: any) => {
    // console.log(data);

    this.listado = data;

    });
  }

  ngOnInit() {

    // console.log ( this.peliculas );
  }

}
