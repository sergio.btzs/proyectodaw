﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using proyecto.Models;

namespace proyecto.DB
{
    public class WebAPI
    {
        public static Mascota BuscarPorId()
        {
            Mascota InformacionMascota = new Mascota();
            DataTable _consulta = new DataTable();
            try
            {
                //_consulta = DBConn.ConsultaSQL("select * from Usuarios where idusuario = "+IdUsuario+"");
                _consulta = DBConn.ConsultaSQL($"select * from mascotas");
                if (_consulta.Rows.Count > 0)
                {
                    InformacionMascota.id = Convert.ToInt32(_consulta.Rows[0].ItemArray[0]);
                    InformacionMascota.nombre = _consulta.Rows[0].ItemArray[1].ToString();
                    InformacionMascota.especie = _consulta.Rows[0].ItemArray[2].ToString();
                    //InformacionMascota.genero = _consulta.Rows[0].ItemArray[3].ToString();
                    InformacionMascota.genero = "bonita";
                }
            }
            catch (Exception)
            {
                InformacionMascota.id = 0;
                InformacionMascota.nombre = "";
                InformacionMascota.especie = "";
                InformacionMascota.genero = "";
            }
            return InformacionMascota;
        }
    }
}