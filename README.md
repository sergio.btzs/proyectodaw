# Proyecto DAW 2020 de Sergio Vázquez Martínez

### Índice
-  [Backend - API REST en C#](#backend)
-  [Frontend - Angular 9](#frontend)
-  [Base de datos - SQL Server](#baseDeDatos)
-  [APIS externas](#apisExternas)
-  [Herramientas génericas](#herramientas)
---

# BackEnd
## API Rest en C#
### Compilar proyecto:
> dotnet build
Se genera una carpeta con el nombre netcoreapp3.1 que es la que tenemos que copiar al servidor.
Contiene el servicio que tendrá que estar activo en el servidor a la espera de que sus métodos sean llamados por la parte del front (Angular).

-  [Información general sobre la CLI de .Net Core](https://docs.microsoft.com/es-es/dotnet/core/tools/)
-  [Swagger API propia](https://localhost:5001/index.html) *Tiene que estar el servicio activo para que se muestren los métodos y poder probarlos.

### Enlaces de interés
-  [Dotnet CLI](https://docs.microsoft.com/es-es/dotnet/core/tools/?tabs=netcore2x)
-  [C#](https://docs.microsoft.com/es-es/dotnet/csharp/)
-  [Swagger](https://swagger.io/solutions/api-documentation/)
-  [Postman](https://www.getpostman.com/)
---

# FrontEnd
## Angular 9
### Compilar proyecto:
> ng build --prod
Se genera una carpeta con el nombre dist que es la que tenemos que copiar al servidor

-  [Comandos básicos Angular](https://angular.io/cli)
  
### Enlaces de interés
-  [Angular 9](https://angular.io/docs)
-  [TypeScript](https://www.typescriptlang.org/docs/home.html)
- [Páginas SPA](https://medium.com/@davidjguru/single-page-application-un-viaje-a-las-spa-a-trav%C3%A9s-de-angular-y-javascript-337a2d18532)
-  [Auth0/](https://auth0.com/docs/)
-  [Bootstrap 4.0](https://getbootstrap.com/docs/4.0/getting-started/introduction/)
-  [SweetAlert2](https://sweetalert2.github.io/)
---

# BaseDeDatos
## SQL Server
-  [Instalación y configuración SQL Server Express](https://docs.microsoft.com/es-es/sql/database-engine/install-windows/install-sql-server?view=sql-server-ver15)
### Enlaces de interés
-  [SQL Server](https://docs.microsoft.com/es-es/sql/sql-server/?view=sql-server-ver15)
-  [Azure Data Studio](https://docs.microsoft.com/es-es/sql/azure-data-studio/?view=sql-server-ver15)
-  [Microsoft SQL Server Management Studio](https://docs.microsoft.com/es-es/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15)
---

# ApisExternas
## Spotify y The Movie DataBase
-  [Spotify](https://developer.spotify.com/console/)
-  [The Movie DataBase](https://developers.themoviedb.org/3/getting-started/introduction)
---

# Herramientas
## Herramientas génericas usadas en el proyecto
-  [Visual Studio Code](https://code.visualstudio.com/)
-  [GitLab](https://gitlab.com/)
-  [Git por línea de comandos](https://git-scm.com/download/win)
-  [Git desde Visual Studio Code](https://code.visualstudio.com/docs/editor/versioncontrol)
---