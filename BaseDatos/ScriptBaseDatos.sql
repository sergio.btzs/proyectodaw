/*
Script Base de Datos Proyecto DAW
Autor: Sergio Vázquez Martínez
Fecha: Mayo 2020
*/
-- CREATE DATABASE proyecto
-- go
USE proyecto
go

/* TABLAS */
-- IF OBJECT_ID('Usuarios', 'U') IS NOT NULL
--   DROP TABLE Usuarios;
-- go
/* Tabla para guardar info de los usuarios */
create table Usuarios
(
Id int primary key not null identity(1,1),
IdAuth0 varchar (255) not null,
Nombre varchar(255) null,
Apellidos varchar(255) null,
Correo varchar(255) null,
Movil varchar (255) null,
Notificaciones bit null,
Borrado int null ,
FechaAlta date null,
FechaModificacion date null,
FechaBorrado date null,
);
go
/***************************************/

/* PROCEDIMIENTOS */
if exists (select 1 from sys.procedures
            where Name = 'getUsuario')
BEGIN
    drop procedure getUsuario
END
go  
/* Descripción: lista los datos de un usuario por su IdAuth0 */
CREATE PROCEDURE getUsuario
  @IdAuth0 varchar(255)
AS
BEGIN
  DECLARE
    @error int
    , @mensaje varchar(255)

  if exists (select id from Usuarios where IdAuth0=@IdAuth0 and Borrado=0)
    BEGIN
        select  IdAuth0 as 'idAuth0'
              , Nombre as 'nombre'
              , Apellidos as 'apellidos'
              , Correo as 'correo'
              , Movil as 'movil'
              , Notificaciones as 'notificaciones'
        from Usuarios where IdAuth0=@IdAuth0 and Borrado=0
    END
  ELSE
    BEGIN
      set @error= 1
      set @mensaje = 'No existe ningún usuario con el idAuth0: ' + @IdAuth0

      select @error as 'error', @mensaje as 'mensaje'
    END
END
GO
/***************************************/

if exists (select 1 from sys.procedures
            where Name = 'agregarUsuario')
BEGIN
    drop procedure agregarUsuario
END
go  
/* Descripción: vincula datos de usuario con su idAuth0 */
CREATE PROCEDURE agregarUsuario
  @IdAuth0 varchar(255)
  , @Nombre varchar(255)
  , @Apellidos varchar(255)
  , @Correo varchar(255)
  , @Movil varchar(255)
  , @Notificaciones bit
AS
BEGIN
  DECLARE
    @error varchar(255)
    , @mensaje varchar(255)

  SET @error = '0'
  SET @mensaje = ''

  if not exists (select id from Usuarios where IdAuth0=@IdAuth0 and Borrado=0)
    BEGIN
      insert into Usuarios (IdAuth0, Nombre, Apellidos, Correo, Movil, Notificaciones,Borrado, FechaAlta, FechaModificacion, FechaBorrado) 
                    values (@IdAuth0, @Nombre, @Apellidos, @Correo, @Movil, @Notificaciones, 0, GETDATE(), null, null)
      set @mensaje = 'El usuario idAuth0: ' + cast(@IdAuth0 as varchar) + ' ha sido añadido'
    END
  ELSE
    BEGIN
      set @error= '1'
      set @mensaje = 'Error, ya existe el idAuth0: ' + @IdAuth0
    END                    
  select @error as 'error', @mensaje as 'mensaje'
  END
GO
/***************************************/

if exists (select 1 from sys.procedures
            where Name = 'modificarUsuario')
BEGIN
    drop procedure modificarUsuario
END
go  
/* Descripción: modifica usuario por su IdAuth0 */
CREATE PROCEDURE modificarUsuario
  @IdAuth0 varchar(255)
  , @Nombre varchar(255)
  , @Apellidos varchar(255)
  , @Correo varchar(255)
  , @Movil varchar(255)
  , @Notificaciones bit  
AS
BEGIN
  DECLARE
    @error varchar(255)
    , @mensaje varchar(255)

  SET @error = '0'
  SET @mensaje = ''

  if exists (select id from Usuarios where IdAuth0=@IdAuth0 and Borrado=0)
    BEGIN
      update Usuarios 
        set Nombre=coalesce(@Nombre,Nombre)
            , Apellidos = coalesce(@Apellidos, Apellidos)
            , Correo = coalesce(@Correo, Correo)
            , Movil = coalesce(@Movil, Movil)
            , Notificaciones = coalesce(@Notificaciones, Notificaciones)
            , FechaModificacion = GETDATE()
      where IdAuth0=@IdAuth0
      set @mensaje = 'El usuario idAuth0: ' + @IdAuth0 + ' ha sido modificado'
    END
  ELSE
    BEGIN
      set @error= '1'
      set @mensaje = 'Error, no existe el idAuth0: ' + @IdAuth0
    END                    
  select @error as 'error', @mensaje as 'mensaje'    
END
GO
/***************************************/

if exists (select 1 from sys.procedures
            where Name = 'eliminarUsuario')
BEGIN
    drop procedure eliminarUsuario
END
go 
/* Descripción: marca usuario como borrado por su IdAuth0 */
CREATE PROCEDURE eliminarUsuario
  @IdAuth0 varchar(255)
AS
BEGIN
  DECLARE
    @error varchar(255)
    , @mensaje varchar(255)

  SET @error = '0'
  SET @mensaje = ''

  if exists (select id from Usuarios where IdAuth0=@IdAuth0 and Borrado=0)
    BEGIN  
      update Usuarios 
        set Borrado=1
        , FechaBorrado = GETDATE()
      where IdAuth0=@IdAuth0
      set @mensaje = 'El usuario idAuth0: ' + @IdAuth0 + ' ha sido borrado'
    END
  ELSE
    BEGIN
      set @error= '1'
      set @mensaje = 'Error, no existe el idAuth0: ' + @IdAuth0
    END                    
  select @error as 'error', @mensaje as 'mensaje'    
END
GO
/***************************************/

