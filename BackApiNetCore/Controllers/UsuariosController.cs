using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using BackApiNetCore.Models;
using BackApiNetCore.BD;
using Microsoft.AspNetCore.Cors;
using System.Data;
using Microsoft.Extensions.Logging;

namespace BackApiNetCore.Controllers
{
    /*
    *
    * proyecto Daw    
    * autor: Sergio Vázquez
    * fecha: Mayo 2020
    *
    * métodos:
    *   - detalle: muestra datos de un usuario por su idAuth0
    *   - agregar: añade un usuario vinculado a un idAuth0
    *   - modificar: modifica un usuario por su idAuth0
    *   - eliminar: marca como borrado un usuario por su idAuth0
    *
    */

    [Route("~/usuario")]
    public class UsuariosController : Controller
    {
        DateTime fecha = DateTime.Now;
        string path = @"C:\Users\sergi\Desktop\GitLab_proyectoDAW\proyectodaw\"
                    + @"BackApiNetCore\Logs\logsAPI.txt";

        // Método que devuelve los datos de un usuario por su idAuth0
        [HttpPost("detalle")]
        public object getUsuario ([FromBody] CDetalleUsuario datosUsuario )
        {
            try
            {
            // Escribir en log
            using (StreamWriter sw = System.IO.File.AppendText(this.path))
            {
                sw.WriteLine(this.fecha + " - " + "[SOL] usuarios/detalle");
                sw.Close();
            }	
        
                CRespuesta respuesta = new CRespuesta();
                Usuario InfoUsuario = new Usuario();
                DataTable listado = CBDUsuarios.getUsuarioById(datosUsuario.IdAuth0);

                if (listado.Columns.Contains("error"))
                {
                    respuesta.error = listado.Rows[0]["error"].ToString();
                    respuesta.mensaje = listado.Rows[0]["mensaje"].ToString();
                    
                    // Escribir en log
                    using (StreamWriter sw = System.IO.File.AppendText(this.path))
                    {
                        sw.WriteLine(this.fecha + " - " + "[RES] usuarios/detalle: "
                                                        + respuesta.mensaje);
                        sw.Close();                        
                    }	

                    return respuesta;
                }
                else
                {
                    if (listado.Rows.Count > 0)
                    {
                        InfoUsuario.IdAuth0 = listado.Rows[0]["idAuth0"].ToString();
                        InfoUsuario.Nombre = listado.Rows[0]["nombre"].ToString();
                        InfoUsuario.Apellidos = listado.Rows[0]["apellidos"].ToString();
                        InfoUsuario.Correo = listado.Rows[0]["correo"].ToString();
                        InfoUsuario.Movil = listado.Rows[0]["movil"].ToString();
                        InfoUsuario.Notificaciones = bool.Parse(listado.Rows[0]["Notificaciones"].ToString());
                    }
                    var jsonConver = JsonConvert.SerializeObject(InfoUsuario);

                    // Escribir en log
                    using (StreamWriter sw = System.IO.File.AppendText(this.path))
                    {
                        sw.WriteLine(this.fecha + " - " + "[RES] usuarios/detalle: "
                                                        + jsonConver);
                        sw.Close();                        
                    }	

                    return jsonConver.ToString();
                }    
            }
            catch (Exception ex)
            {
                // Escribir en log
                using (StreamWriter sw = System.IO.File.AppendText(this.path))
                {
                    sw.WriteLine(this.fecha + " - " + "[RES] usuarios/detalle - Excepcion: "
                                                    + ex.Message + " _ " + ex.StackTrace);
                    sw.Close();                     
                }	
                throw ex;
            }
        }

        // Método que agrega los datos de un usuario a la BD vinculado a su idAuth0
        [HttpPost("agregar")]
        public object agregarUsuario ([FromBody] Usuario datosUsuario )
        {
            // Escribir en log
            using (StreamWriter sw = System.IO.File.AppendText(this.path))
            {
                sw.WriteLine(this.fecha + " - " + "[SOL] usuarios/agregar");
                sw.Close();                
            }	

            CRespuesta respuesta = new CRespuesta();
            DataTable listado = CBDUsuarios.agregarUsuario(datosUsuario.IdAuth0
                                                ,datosUsuario.Nombre
                                                ,datosUsuario.Apellidos
                                                , datosUsuario.Correo
                                                , datosUsuario.Movil
                                                , datosUsuario.Notificaciones);                                                
            try
            {
                respuesta.error = listado.Rows[0]["error"].ToString();
                respuesta.mensaje = listado.Rows[0]["mensaje"].ToString();


                // Escribir en log
                using (StreamWriter sw = System.IO.File.AppendText(this.path))
                {
                    sw.WriteLine(this.fecha + " - " + "[RES] usuarios/agregar: "
                                                    + respuesta.mensaje);
                    sw.Close();                    
                }	

                var jsonConver = JsonConvert.SerializeObject(respuesta);
                return jsonConver.ToString();
            }
            catch (Exception ex)
            {
                // Escribir en log
                using (StreamWriter sw = System.IO.File.AppendText(this.path))
                {
                    sw.WriteLine(this.fecha + " - " + "[RES] usuarios/agregar - Excepcion: "
                                                    + ex.Message + " _ " + ex.StackTrace);
                    sw.Close();                     
                }	
                throw ex;
            }
        }

        // Método que modifica los datos de un usuario en la BD vinculado a su idAuth0
       [HttpPost("modificar")]
        public object modificarUsuario ([FromBody] Usuario datosUsuario )
        {
            // Escribir en log
            using (StreamWriter sw = System.IO.File.AppendText(this.path))
            {
                sw.WriteLine(this.fecha + " - " + "[SOL] usuarios/modificar");
                sw.Close();                
            }	

            CRespuesta respuesta = new CRespuesta();
            DataTable listado = CBDUsuarios.modificarUsuario(datosUsuario.IdAuth0
                                                , datosUsuario.Nombre
                                                , datosUsuario.Apellidos
                                                , datosUsuario.Correo
                                                , datosUsuario.Movil
                                                , datosUsuario.Notificaciones);
            try
            {
                respuesta.error = listado.Rows[0]["error"].ToString();
                respuesta.mensaje = listado.Rows[0]["mensaje"].ToString();
                
                // Escribir en log
                using (StreamWriter sw = System.IO.File.AppendText(this.path))
                {
                    sw.WriteLine(this.fecha + " - " + "[RES] usuarios/modificar: "
                                            + respuesta.mensaje);
                    sw.Close();                    
                }	

                var jsonConver = JsonConvert.SerializeObject(respuesta);
                return jsonConver.ToString();
            }
            catch (Exception ex)
            {
                // Escribir en log
                using (StreamWriter sw = System.IO.File.AppendText(this.path))
                {
                    sw.WriteLine(this.fecha + " - " + "[RES] usuarios/modificar - Excepcion: "
                                                    + ex.Message + " _ " + ex.StackTrace);
                    sw.Close();                     
                }	
                throw ex;
            }
        }

        // Método que marca como borrado un usuario por su idAuth0
       [HttpPost("eliminar")]
        public object eliminarUsuario ([FromBody] CDetalleUsuario datosUsuario )
        {
            // Escribir en log
            using (StreamWriter sw = System.IO.File.AppendText(this.path))
            {
                sw.WriteLine(this.fecha + " - " + "[SOL] usuarios/eliminar");
                sw.Close();
            }	

            CRespuesta respuesta = new CRespuesta();
            DataTable listado = CBDUsuarios.eliminarUsuario(datosUsuario.IdAuth0);
            try
            {
                respuesta.error = listado.Rows[0]["error"].ToString();
                respuesta.mensaje = listado.Rows[0]["mensaje"].ToString();
                
                // Escribir en log
                using (StreamWriter sw = System.IO.File.AppendText(this.path))
                {
                    sw.WriteLine(this.fecha + " - " + "[RES] usuarios/eliminar: " 
                                                            + respuesta.mensaje);
                    sw.Close();
                }	

                var jsonConver = JsonConvert.SerializeObject(respuesta);
                return jsonConver.ToString();
            }
            catch (Exception ex)
            {
                // Escribir en log
                using (StreamWriter sw = System.IO.File.AppendText(this.path))
                {
                    sw.WriteLine(this.fecha + " - " + "[RES] usuarios/eliminar - Excepcion: "
                                                    + ex.Message + " _ " + ex.StackTrace);
                    sw.Close();                     
                }	
                throw ex;
            }
        }

    }
}