﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace BackApiNetCore.BD
{
    public class DBConn
    {
        private static string path = @"C:\Users\sergi\Desktop\GitLab_proyectoDAW\" 
                            + @"proyectodaw\BackApiNetCore\Logs\logsAPI.txt";
        
        // se establece conexión con la BD
        public static SqlConnection ConexionSQL()
        {
            SqlConnection ConectString = new SqlConnection("Data Source=VASK0\\SQLEXPRESS;" +
                                                                "Initial Catalog=proyecto;" + 
                                                                "User id=proyecto;" +
                                                                "Password=123456;");
            return ConectString;
        }

        // se crea la consulta
        public static DataTable ConsultaSQL(string Query)
        {
            DateTime fecha = DateTime.Now;
            DataTable Consulta = new DataTable();
            SqlConnection conn = new SqlConnection();
            conn = ConexionSQL();
            SqlCommand cmd = new SqlCommand(Query, conn);
            try
            {
                if (conn.State == System.Data.ConnectionState.Open)
                {
                    conn.Close();
                }
                else
                {
                    conn.Open();
                }
                Consulta.Load(cmd.ExecuteReader());
            }
            catch (Exception ex)
            {
                // Escribir en log
                using (StreamWriter sw = System.IO.File.AppendText(path))
                {
                    sw.WriteLine(fecha + " - " + "[BD] Conexión BD - Exception: "
                                               + ex.Message + " _ " + ex.StackTrace);
                    sw.Close();                
                }	
            }
            // se cierra conexión con BD
            finally
            {
                conn.Close();
            }
            return Consulta;
        }
    }
}