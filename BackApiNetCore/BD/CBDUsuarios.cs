﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using BackApiNetCore.Models;

namespace BackApiNetCore.BD
{
    public class CBDUsuarios
    {    
        private static string path = @"C:\Users\sergi\Desktop\GitLab_proyectoDAW\" 
                                    + @"proyectodaw\BackApiNetCore\Logs\logsAPI.txt";

        // establece conexión con BD y ejecuta Procedimiento Almacenado "getUsuario"
        public static DataTable getUsuarioById(string IdAuth0)
        {
            DateTime fecha = DateTime.Now;
            DataTable consulta = new DataTable();
            try
            {
                consulta = DBConn.ConsultaSQL(
                    String.Format("getUsuario '{0}'", IdAuth0)
                    );
            }
            catch (Exception ex)
            {
                // Escribir en log
                using (StreamWriter sw = System.IO.File.AppendText(path))
                {
                    sw.WriteLine(fecha + " - " + "[BD] getUsuarioById - Exception: "
                                             + ex.Message + " _ " + ex.StackTrace);
                    sw.Close();                
                }	
                throw ex;
            }
            
            return consulta;
        }

        // establece conexión con BD y ejecuta PA "agregarUsuario"
       public static DataTable agregarUsuario(string IdAuth0
                                            , string Nombre
                                            , string Apellidos
                                            , string Correo
                                            , string Movil
                                            , bool? Notificaciones)
        {
            DateTime fecha = DateTime.Now;
            DataTable consulta = new DataTable();
            try
            {
                consulta = DBConn.ConsultaSQL(
                    String.Format("agregarUsuario '{0}','{1}','{2}','{3}','{4}',{5}", 
                                                            IdAuth0
                                                            , Nombre
                                                            , Apellidos
                                                            , Correo
                                                            , Movil
                                                            , Notificaciones.ToString()
                                                            )
                    );
            }
            catch (Exception ex)
            {
                // Escribir en log
                using (StreamWriter sw = System.IO.File.AppendText(path))
                {
                    sw.WriteLine(fecha + " - " + "[BD] agregarUsuario - Exception: " 
                                            + ex.Message + " _ " + ex.StackTrace);
                    sw.Close();                
                }	
                throw ex;
            }

            return consulta;
        }

        // establece conexión con BD y ejecuta PA "modificarUsuario"
       public static DataTable modificarUsuario(string IdAuth0
                                              , string Nombre
                                              , string Apellidos
                                              , string Correo
                                              , string Movil
                                              , bool? Notificaciones)
        {
            DateTime fecha = DateTime.Now;            
            DataTable consulta = new DataTable();
            try
            {
                consulta = DBConn.ConsultaSQL(
                    String.Format("modificarUsuario '{0}',{1},{2},{3},{4},{5}", 
                        IdAuth0
                        , Nombre == null ? "null" : Convert.ToString("'"+ Nombre +"'")
                        , Apellidos == null ? "null" : Convert.ToString("'"+ Apellidos +"'")
                        , Correo == null ? "null" : Convert.ToString("'"+ Correo +"'")
                        , Movil == null ? "null" : Convert.ToString("'"+ Movil +"'")
                        , Notificaciones == null ? "null" : Notificaciones.ToString()
                        )
                    );
            }
            catch (Exception ex)
            {
                // Escribir en log
                using (StreamWriter sw = System.IO.File.AppendText(path))
                {
                    sw.WriteLine(fecha + " - " + "[BD] modificarUsuario - Exception: " 
                                            + ex.Message + " _ " + ex.StackTrace);
                    sw.Close();                
                }	
                throw ex;
            }

            return consulta;
        }

        // establece conexión con BD y ejecuta PA "eliminarUsuario"
       public static DataTable eliminarUsuario(string IdAuth0)
        {
            DateTime fecha = DateTime.Now;            
            DataTable consulta = new DataTable();
            try
            {
                consulta = DBConn.ConsultaSQL(
                    String.Format("eliminarUsuario '{0}'", IdAuth0)
                    );
            }
            catch (Exception ex)
            {
                // Escribir en log
                using (StreamWriter sw = System.IO.File.AppendText(path))
                {
                    sw.WriteLine(fecha + " - " + "[BD] eliminarUsuario - Exception: " 
                                            + ex.Message + " _ " + ex.StackTrace);
                    sw.Close();                
                }	
                throw ex;
            }

            return consulta;
        }

    }
}