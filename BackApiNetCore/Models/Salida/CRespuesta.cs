using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackApiNetCore.Models
{
    public class CRespuesta
    {
        public string error { get; set; }
        public string mensaje { get; set; }
    }
}