using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackApiNetCore.Models
{
    public class Usuario
    {
        public string IdAuth0 { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Correo { get; set; }
        public string Movil { get; set; }
        public bool? Notificaciones { get; set; }
    }
}